﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Phase_1_Project_Team_3.Models
{
    public class InventoryItem
    {

        public int Id { get; set; }
        public int CreatedByUserId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

    }
}