﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Phase_1_Project_Team_3.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Phase_1_Project_Team_3.Models
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("ProjectContext")
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}