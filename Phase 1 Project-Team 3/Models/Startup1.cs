﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;

using Owin;

[assembly: OwinStartup(typeof(Phase_1_Project_Team_3.Models.Startup1))]

namespace Phase_1_Project_Team_3.Models
{
    public class Startup1
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
