﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phase_1_Project_Team_3.Models
{
    public class User
    {
       
        public int Id { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [StringLength(150)]
        [Display(Name = "User name")]
        public string Username { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "First Name")]
        public string FirstName {get;set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [StringLength(150,MinimumLength =6)]
        [DataType(DataType.Password)]
        //[Display(Name = "Password")]
        public string Password { get; set; }
    }
   
}