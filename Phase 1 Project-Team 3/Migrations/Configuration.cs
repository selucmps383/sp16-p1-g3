namespace Phase_1_Project_Team_3.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<Phase_1_Project_Team_3.Models.ProjectContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Phase_1_Project_Team_3.Models.ProjectContext";
        }

        protected override void Seed(Phase_1_Project_Team_3.Models.ProjectContext context)
        {
            context.Users.AddOrUpdate(x => x.Id, new User { Id = 1, FirstName = "Admin",LastName = "User", Username = "admin",  Password = Crypto.HashPassword("selu2014")} );
            context.InventoryItems.AddOrUpdate(x => x.Id, new InventoryItem {CreatedByUserId = 2,Name = "Jessie", Quantity = 5});

        }
    }
}
