namespace Phase_1_Project_Team_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTableChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Salt", c => c.String());
            CreateIndex("dbo.User", "Username", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.User", new[] { "Username" });
            DropColumn("dbo.User", "Salt");
        }
    }
}
