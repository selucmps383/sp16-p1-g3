﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.SignalR;
using Phase_1_Project_Team_3.Models;

namespace Phase_1_Project_Team_3.Hubs
{
    public class InventoryHub : Hub
    {
        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<InventoryHub>();

        public static void Send(InventoryItem Item)
        {
            hubContext.Clients.All.updating(Item);

        }

        public static void Delete(InventoryItem deleteItem)
        {
            
            hubContext.Clients.All.delete(deleteItem);
        }
    }
}