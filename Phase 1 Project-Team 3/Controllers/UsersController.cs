﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Phase_1_Project_Team_3.Models;
using System.Web.Helpers;
using System.Web.Security;
using System.Data.Entity.Migrations;

namespace Phase_1_Project_Team_3.Controllers
{
    public class UsersController : Controller
    {
        private ProjectContext db = new ProjectContext();

        [Authorize]
        public ActionResult Index()
        {
            return View(db.Users.Where(u=> u.Username!="admin").ToList());
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Models.User User)
        {

            if (IsValid(User.Username, User.Password))
            {
                User currentUser = db.Users.FirstOrDefault(u => u.Username == User.Username);
                if (currentUser != null) Session["CurrentUserId"] = currentUser.Id;

                FormsAuthentication.SetAuthCookie(User.Username, true);
                return RedirectToAction("Index", "Users");
            }
            ModelState.AddModelError("", "Login information is incorrect.");
            return View(User);
        }

        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }


        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.User user)
        {
            bool userNameInUse = db.Users.Any(u => u.Username.Equals(user.Username));
            if (userNameInUse == true)
            {
                ModelState.AddModelError("", "Username in use.");


            }
            else {

                user.Password = Crypto.HashPassword(user.Password);
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(user);
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,FirstName,LastName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Password = Crypto.HashPassword(user.Password);
                db.Users.AddOrUpdate(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        private bool IsValid(string userName, string password)
        {
            bool IsValid = false;

            var user = db.Users.FirstOrDefault(u => u.Username.Equals(userName));

            if (user != null && Crypto.VerifyHashedPassword(user.Password, password))
            {
                IsValid = true;
            }
            return IsValid;
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}