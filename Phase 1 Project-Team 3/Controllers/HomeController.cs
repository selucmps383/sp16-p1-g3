﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Phase_1_Project_Team_3.Models;

namespace Phase_1_Project_Team_3.Controllers
{
    public class HomeController : Controller
    {
        ProjectContext db =new ProjectContext();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        
        
        public ActionResult BuyItem(int? id)
        {
            
            return View(db.InventoryItems.FirstOrDefault(u=>u.Id == id));
        }
    }
}
