﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.SignalR;
using Phase_1_Project_Team_3.Models;

namespace Phase_1_Project_Team_3.Controllers
{
    public class InventoryController : ApiController
    {
        private ProjectContext db = new ProjectContext();

        // GET: api/Inventory
        public IQueryable<InventoryItem> GetInventoryItems()
        {
            return db.InventoryItems;
        }

        // GET: api/Inventory/5
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult GetInventoryItem(int id)
        {
            InventoryItem inventoryItem = db.InventoryItems.Find(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return Ok(inventoryItem);
        }

        // PUT: api/Inventory/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInventoryItem(InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

         

            db.Entry(inventoryItem).State = EntityState.Modified;
            InventoryItem selectedItem = db.InventoryItems.FirstOrDefault(u => u.Id == inventoryItem.Id);
            if (InventoryItemExists(inventoryItem.Id) && (selectedItem.Quantity >= 0 && inventoryItem.Quantity >= 0))
            {
                selectedItem.Quantity = selectedItem.Quantity;
                Hubs.InventoryHub.Send(selectedItem);
                db.InventoryItems.AddOrUpdate(selectedItem);
                db.SaveChanges();


                return Ok();
            }
            else
            {
                return BadRequest("Inventory quantity has been depleted");
            }

        }
            /*try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryItemExists(inventoryItem.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }*/

        // POST: api/Inventory
        [ResponseType(typeof(InventoryItem))]
        public IHttpActionResult PostInventoryItem(InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            InventoryItem selectedItem = db.InventoryItems.FirstOrDefault(u => u.Id == inventoryItem.Id);
            if (InventoryItemExists(inventoryItem.Id)&& (selectedItem.Quantity >0 && inventoryItem.Quantity > 0))
            {
                selectedItem.Quantity = selectedItem.Quantity - inventoryItem.Quantity;
                Hubs.InventoryHub.Send(selectedItem);
                db.InventoryItems.AddOrUpdate(selectedItem);
                db.SaveChanges();


                return Ok();
            }
            else
            {
                return BadRequest("Inventory quantity has been depleted");
            }

        }

        // DELETE: api/Inventory/5
        [ResponseType(typeof(InventoryItem))]
        public async Task<IHttpActionResult> DeleteInventoryItem(int id)
        {
            InventoryItem inventoryItem = await db.InventoryItems.FindAsync(id);
            if (inventoryItem == null)
            {
                return NotFound();
            }
            Hubs.InventoryHub.Delete(inventoryItem);
            db.InventoryItems.Remove(inventoryItem);
            await db.SaveChangesAsync();

            return Ok(inventoryItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryItemExists(int id)
        {
            return db.InventoryItems.Count(e => e.Id == id) > 0;
        }
    }
}